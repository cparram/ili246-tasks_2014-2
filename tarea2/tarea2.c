#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <string.h>
#include <sys/types.h>
#include <sys/sysinfo.h>

int main(int argc, char * argv[]) {
    
    /* Number of times each thread will analyze the system and waiting 
    time between each analysis */
    int times, seconds_to_sleep;
    
    printf("Cantidad de veces que cada subproceso analizará el sistema: ");
    scanf("%d", &times);
    
    printf("Tiempo de espera entre cada análisis: ");
    scanf("%d", &seconds_to_sleep);
    
    /* semaphores to subprocess */
    sem_t *semafore1 = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, 
        MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sem_t *semafore2 = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, 
        MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sem_t *semafore3 = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, 
        MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sem_init(semafore1, 1, 1);
    sem_init(semafore2, 1, 0);
    sem_init(semafore3, 1, 0);
    
    /* semaphore to father can acces to the shered memory */
    sem_t *semafore_father1 = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, 
        MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sem_t *semafore_father2 = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, 
        MAP_SHARED | MAP_ANONYMOUS, -1, 0);                            
    sem_t *semafore_father3 = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, 
        MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sem_init(semafore_father1, 1, 0);
    sem_init(semafore_father2, 1, 0);
    sem_init(semafore_father3, 1, 0);
    
    /* shared memory */
    char *sh_mem = mmap(NULL, sizeof(char) * 100, PROT_READ | PROT_WRITE, 
        MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    strcpy(sh_mem, "");
    
     /* for the children process */
    int pid1, pid2, pid3; 
    
    pid1 = fork();
    if (pid1 == 0) { /* subprocess #1; time in seconds */
        int i;
        for (i = 0; i < times; i++) {
            sem_wait(semafore1);
            
            //get the time in seconds and send it to the shared memory
            struct sysinfo info;
            sysinfo(&info);
            
            if (i == times - 1){
                sprintf(sh_mem, "%d: El sistema lleva %ld s encendido.\n%d: Subproceso finalizado.\n", 
                    getpid(), info.uptime, getpid());
            } else {
                sprintf(sh_mem, "%d: El sistema lleva %ld s encendido.\n", getpid(), 
                    info.uptime);
            }
            sleep(seconds_to_sleep);
            sem_post(semafore_father1);
        }
        exit(0); 
    } 
    
    pid2 = fork();
    if (pid2 == 0) { /* subprocess #2; RAM available */ 
        int i; 
        for (i = 0; i < times; i++) {
            sem_wait(semafore2);
            
            //get the RAM available and send it to the shared memory
            struct sysinfo info;
            sysinfo(&info);
            
            if (i == times - 1){
                sprintf(sh_mem, "%d: Disponible %ldMB de %ldMB.\n%d: Subproceso finalizado.\n",
                    getpid(), (info.totalram-info.freeram)/1048576, 
                    info.totalram/1048576, getpid());
            } else {
                sprintf(sh_mem, "%d: Disponible %ldMB de %ldMB.\n", getpid(), 
                    (info.totalram-info.freeram)/1048576, info.totalram/1048576);
            }
            sleep(seconds_to_sleep);
            sem_post(semafore_father2);
        }  
        exit(0); 
    }
    
    pid3 = fork();
    if (pid3 == 0) { /* subprocess #3; number of processes running */ 
        int i; 
        for (i = 0; i < times; i++) {
            sem_wait(semafore3);
            
            //get the number of processes running and send it to the shared memory
            struct sysinfo info;
            sysinfo(&info);
            
            if (i == times - 1){
                sprintf(sh_mem, "%d: Hay %d procesos en ejecución.\n%d: Subproceso finalizado.\n", 
                    getpid(),  info.procs, getpid());
            } else {
                sprintf(sh_mem, "%d: Hay %d procesos en ejecución.\n",
                    getpid(), info.procs);
            }
            sleep(seconds_to_sleep);
            sem_post(semafore_father3);
        }
        exit(0); 
    } 
    
    if (pid1 < 0 || pid2 < 0 || pid3 < 0) { 
        printf("Problems in creating subprocesses\n"); 
        exit (1); 
    } 
    
    if (pid1 > 0 && pid2 > 0 && pid3 > 0) { 
        int i;
        FILE *file;
        
        for (i = 0; i < times; i++){
            sem_wait(semafore_father1);
            file = fopen("log.txt", "a");
            fputs(sh_mem, file);
            fclose(file);
            sem_post(semafore2);
            
            sem_wait(semafore_father2);
            file = fopen("log.txt", "a");
            fputs(sh_mem, file);
            fclose(file);
            sem_post(semafore3);
            
            sem_wait(semafore_father3);
            file = fopen("log.txt", "a");
            fputs(sh_mem, file);
            fclose(file);
            sem_post(semafore1);
        } 
        /* the three children finished */ 
        file = fopen("log.txt", "a");
        fprintf(file, "%d: Programa finalizado.\n", getpid());
        fclose(file);
    }
    
    munmap(semafore1, sizeof(sem_t));
    munmap(semafore2, sizeof(sem_t));
    munmap(semafore3, sizeof(sem_t));
    munmap(semafore_father1, sizeof(sem_t));
    munmap(semafore_father2, sizeof(sem_t));
    munmap(semafore_father3, sizeof(sem_t));
    
    munmap(sh_mem, sizeof(int));
    
    return 0;
}