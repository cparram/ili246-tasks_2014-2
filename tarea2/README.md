#Tarea 2 `SO_2014-2`

## Resumen

El programa **crea subprocesos** mediante utilizando `fork()`. Cada subproceso se encarga de obtener información sobre el estado del sistema que luego comparte, por medio de un espacio de **memoria compartida** con el proceso padre. Todo esto es sincronizado a través de **semáforos**.

## Funcionamiento

El programa comienza esperando el input por parte del usuario; cantidad de veces que cada subproceso analizará el sistema y el tiempo de espera entre cada análisis
Luego, para mejor claridad y menos confución, se crean **6 semáforos**, inicializando su valor con 1 para el primer semáforo y 0 para los restantes. Para ello se utiliza `sem_init(...)`

Se crean 3 subprocesos los cuales obtendrán información sobre el estado del sistema. Para esto, el primer semáforo habilita al primer proceso, luego cuando este ya obtiene el tiempo en segundos en que lleva encendido el sistema, comparte la información en el espacio de memoria compartida. El proceso padre espera que el primer proceso realice tal operación con un `sem_wait(...)`.
Cuando el primer proceso termina, habilita al padre para que este pueda acceder al espacio de memoria compartida y así acceder a la información obtenida por el proceso 1. Luego de ello, el padre registra tal información el `log.txt`. Entonces el padre, después que realiza el registro, habilita al proceso 2 para que comience su ejecución, y así sucesivamente, con el correspondiente tiempo de espera determinado por el usuario al iniciar el programa; se utiliza `sleep()`.
Esto anterior se realiza las veces estimadas por el usuario inicialmente, en el que en cada iteración, cada proceso accede al estado del sistema con un espacio de tiempo determinado.

El programa termina cuando todos los subprocesos terminan su ejecución.

## Uso
* `$ make` para compilar el programa.
* `$ ./tarea2` para ejecutar el programa
* `$ make clean` para eliminar los archivos generados, incluyendo `log.txt`.

## Referencias
* http://www.ual.es/~rguirado/so/practica3.pdf
* http://www.ual.es/~rguirado/so/practica5.pdf
* [semaforos](http://www.chuidiang.com/clinux/ipcs/semaforo.php)
* [sysinfo.h](http://www.scs.stanford.edu/histar/src/pkg/uclibc/include/sys/sysinfo.h)
   * [stackoverflow](http://stackoverflow.com/questions/8987636/sysinfo-system-call-not-returning-correct-freeram-value)