#define _FILE_OFFSET_BITS 64 /* to know the size of large file */

#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h> 

static const char WEIGHT_1[] = "0KB-100KB";
static const char WEIGHT_2[] = "100KB-1MB";
static const char WEIGHT_3[] = "1MB-100MB";
static const char WEIGHT_4[] = "100MB-1GB";
static const char WEIGHT_5[] = "1GB-moreGB";

int main (int argc, char *argv[]){
    
    /* variables to group directory */
    DIR *dir_to_group;
    struct dirent *direntp;
 
    /* argument checking */
    if (argc != 2){
        printf("Use: %s directory\n", argv[0]);
        return 0;
    }

    /* if the argument do not have the final symbol '/' is added */ 
    if (argv[1][strlen(argv[1])-1] != '/'){
        strcat(argv[1], "/");
    }
    
    /* directory opens for grouping */
    dir_to_group = opendir(argv[1]);
    if (dir_to_group == NULL){
        printf("Error: Can not open the directory\n");
        return 1;
    }
    
    /* directory files are read */
    while ((direntp = readdir(dir_to_group)) != NULL) {

        /* pointer with the file extension */
        char *ext;
        ext = strchr(direntp->d_name, '.');
        if (ext == NULL || strcmp(ext, ".") == 0 || strcmp(ext, "..") ==0){
            /* read invalid files. NULL if what you read is a file without 
            extension or directory*/
            continue;
        } 
        /* variable that contains the name of the new directory according 
        to the extension of the file readed */
        char *dir_with_ext_name = malloc(sizeof(char) * 500);
        strcat(dir_with_ext_name, argv[1]);
        strcat(dir_with_ext_name, ext + 1);
        strcat(dir_with_ext_name, "/");        

        /* pointer will be the current address of readed file */
        char *old_path_file_readed = malloc(sizeof(char) * 500);
        strcat(old_path_file_readed, argv[1]);
        strcat(old_path_file_readed, direntp->d_name);

        /* struct to know the state of the file readed */
        struct stat file_stat;
        if (stat(old_path_file_readed, &file_stat) < 0) {
            printf("Failed to get file status. Error #%i. %s\n", errno, strerror(errno));
            return 1;
        } 

        /* the file size is checked to save it in the appropriate folder */
        double file_size = file_stat.st_size/1024.0;

        /* the variable that has the name of the directory corresponding to
        the size of the file is created */
        char *dir_size_name = malloc(sizeof(char) * 500);
        strcat(dir_size_name, dir_with_ext_name);
        
        /* conditional for the directory name corresponding to the file size */                
        if (file_size < 100){
            strcat(dir_size_name, WEIGHT_1);
        } else if (100 < file_size && file_size < 1024) {
            strcat(dir_size_name, WEIGHT_2);
        } else if (1024 < file_size && file_size < 100 * 1024){
            strcat(dir_size_name, WEIGHT_3);
        } else if (100 * 1024 < file_size && file_size < 1024 * 1024){
            strcat(dir_size_name, WEIGHT_4);
        } else {
            strcat(dir_size_name, WEIGHT_5);
        }
        strcat(dir_size_name, "/");

        /* is open the appropriate directory to the file extension */
        DIR *dir_ext = opendir(dir_with_ext_name);
        if (dir_ext == NULL){
             /* problem reading directory corresponding to the readed 
             extension*/ 

            if (errno == 2){ 
                /* the directory does not exist then it is created */
                mkdir(dir_with_ext_name, S_IRUSR | S_IWUSR | S_IXUSR);
                
                /* the new directory to the size of the file is 
                created */                                
                mkdir(dir_size_name, S_IRUSR | S_IWUSR | S_IXUSR);

            } else {
                printf("Error opening directory \'%s\', error #%i. %s\n", ext + 1, errno, strerror(errno));
            }                      
        } else {
            /* no problem opening the directory with the same name as 
            the file extension */

            /* is open the appropriate directory to the file size */
            DIR *dir_size = opendir(dir_size_name);
            if (dir_size == NULL){
                /* problem reading directory corresponding to the size 
                of the file read*/

                if (errno == 2){                     
                    /* the directory does not exist then it is created */                                
                    mkdir(dir_size_name, S_IRUSR | S_IWUSR | S_IXUSR);

                } else {
                    printf("Error opening directory \'%s\', error #%i. %s\n", ext + 1, errno, strerror(errno));
                }  
            }else {
                /* change the readed file to the appropriate directory 
                to size */
            }
            
            /* close the directory */
            dir_size = NULL;
            closedir(dir_size);           
        }

        /* the name of the new path is created to change the file directory */
        char *new_path_file_readed = malloc(sizeof(char) * 500);
        strcat(new_path_file_readed, dir_size_name);
        strcat(new_path_file_readed, direntp->d_name);

        /* change the file directory */
        rename(old_path_file_readed, new_path_file_readed);

        dir_size_name = NULL;
        new_path_file_readed = NULL;
        free(dir_size_name);
        free(new_path_file_readed);

        dir_with_ext_name = NULL;
        old_path_file_readed = NULL;
        free(old_path_file_readed);
        free(dir_with_ext_name);      
    }
 
    /* close the directory */
    dir_to_group = NULL;
    closedir(dir_to_group);
    
    return 0;
}