#Tarea 1 `SO_2014-2`

## Resumen

 El programa es capaz de agrupar los archivos de un directorio, entregado como parámetro al ejecutar el programa, en subdirectorios dentro de esta según la extensión que estos tengan, los subdirectorios tendrán como nombre la extensión de los archivos que contienen. Luego los archivos de los subdirectorios serán agrupados según su
peso.

## Funcionamiento

El programa recibe __un sólo__ parámetro, el cual debe ser un directorio (**inicialmente hay una verificación del correcto uso**). 

El programa abre el directorio entregado como parámetro, con `opendir` y lee cada uno de los archivos que tiene en su interior, con `readdir`. Luego, con `strchr` y `stat` se obtiene la extensión y el tamaño del archivo, respectivamente. 

Entonces, si el directorio con el nombre de la extensión del **archivo leído**: 

*  **No existe**:
    * Se crea con `mkdir` el directorio con el nombre de la extensión del archivo.
    * Se crea con `mkdir` el directorio con el nombre del tamaño correspondiente.
* **Existe**: 
    * Se verifica, con `opendir`,  si existe o no la carpeta con el nombre del tamaño correspondiente. Si no existe, se crea con `mkdir`.

Luego, el archivo leído es movido a la subcarpeta correspondiente a su tamaño que está dentro de la carpeta que lleva por nombre su extensión. 

## Uso
* `$ make`
* `$ ./tarea1 <directorio>`
    * `<directorio>` puede ser de la forma `../folder` o `../folder/`, `folder` o `folder/`

## Referencias
* [POSIX](http://cplus.kompf.de/posixlist.html)
* [stat](http://codewiki.wikidot.com/c:system-calls:stat)
* [Repositorio](http://bit.ly/1vXWHL9)
* Enunciado tarea.
